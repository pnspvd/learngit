/**
 * 
 */
package com.acttest.tests;

import org.testng.annotations.Test;

import com.acttest.pages.HomeTestPage;
import com.acttest.pages.QuizdetailTestPage;
import com.acttest.pages.QuizhomeTestPage;
import com.acttest.pages.SettingTestPage;
import com.acttest.pages.YourcollectionsTestPage;

/**
 * @author satyawan.pawar
 *
 */
public class ACTTests {
	HomeTestPage homeTestPage = new HomeTestPage();
	SettingTestPage settingTestPage = new SettingTestPage();
	QuizhomeTestPage quizhomeTestPage = new QuizhomeTestPage();
	QuizdetailTestPage quizdetailTestPage = new QuizdetailTestPage(); 
	YourcollectionsTestPage yourcollectionsTestPage = new YourcollectionsTestPage();
	
	@Test(enabled=false,priority=1)
	public void resetData() {
		homeTestPage.launchPage(null);
		homeTestPage.verifyHomePage();
		homeTestPage.clickOnTab(7);		
		settingTestPage.waitForPageToLoad();
		settingTestPage.verfiySettingPage();
		settingTestPage.clickOnResetData();
		homeTestPage.verifyTotalScrore();	
		
	}
	
	@Test(enabled=true,priority=2)
	public void unusefulQuestion() {		
		homeTestPage.launchPage(null);
		homeTestPage.verifyHomePage();
		homeTestPage.clickOnTab(1);	
		quizhomeTestPage.verifyQuizHomePage();
		quizhomeTestPage.clickOnStartButton();
		quizdetailTestPage.verifyQuizDetailPage();
		quizdetailTestPage.getQuestionInfo();
		quizdetailTestPage.clickOnBookmarkIcon();
		homeTestPage.scrollToElement(8);
		homeTestPage.clickOnTab(6);		
		yourcollectionsTestPage.selectPreviouslySelectedQuiz(1);
		yourcollectionsTestPage.clickOnView();
		yourcollectionsTestPage.verifyMasteredQuestion();		
	}	
	
	@Test(enabled=false,priority=3)
	public void recentActivity(){
		homeTestPage.launchPage(null);
		homeTestPage.verifyHomePage();
		homeTestPage.clickOnTab(1);	
		quizhomeTestPage.verifyQuizHomePage();
		quizhomeTestPage.clickOnStartButton();
		quizdetailTestPage.verifyQuizDetailPage();
		quizdetailTestPage.handleDailogue();
		homeTestPage.verifyProfile();
		homeTestPage.verifyIncompleteStatus();		
	}
}
