package com.acttest.pages;

import java.util.List;

import org.hamcrest.Matchers;

import com.acttest.utils.DriverUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class HomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "homepage.tablist.btns")
	private List<QAFWebElement> homepageTablistBtns;

	@FindBy(locator = "homepage.profile.icon")
	private QAFWebElement homepageProfileIcon;

	@FindBy(locator = "homepage.profile.verifyscore")
	private QAFWebElement homepageProfileVerifyscore;

	@FindBy(locator = "homepage.profile.username.title.lbl")
	private QAFWebElement homepageProfileUsernameTitleLbl;

	@FindBy(locator = "homepage.profile.username.lbl")
	private QAFWebElement homepageProfileusernameLbl;

	@FindBy(locator = "homepage.profile.incomplete.message")
	private QAFWebElement homepageProfileIncompleteMessage;

	public QAFWebElement getHomepageProfileUsernameTitleLbl() {
		return homepageProfileUsernameTitleLbl;
	}

	public QAFWebElement getHomepageProfileusernameLbl() {
		return homepageProfileusernameLbl;
	}

	public QAFWebElement getHomepageProfileIncompleteMessage() {
		return homepageProfileIncompleteMessage;
	}

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public List<QAFWebElement> getHomepageTablistBtns() {
		return homepageTablistBtns;
	}

	public QAFWebElement getHomepageProfileIcon() {
		return homepageProfileIcon;
	}

	public QAFWebElement getHomepageProfileVerifyscore() {
		return homepageProfileVerifyscore;
	}

	public void verifyHomePage() {
		waitForPageToLoad();
		Validator.verifyThat("Verify ACT Test HomePage", getHomepageTablistBtns().size(), Matchers.greaterThan(0));
	}

	public void clickOnTab(int index) {
		getHomepageTablistBtns().get(index).waitForVisible();
		getHomepageTablistBtns().get(index).click();
	}

	public void scrollToElement(int index) {
		getHomepageTablistBtns().get(index).waitForVisible();
		DriverUtils.getAndroidDriver().scrollTo(getHomepageTablistBtns().get(index).getText());
	}

	public void verifyTotalScrore() {
		getHomepageProfileIcon().waitForVisible();
		getHomepageProfileIcon().click();
		getHomepageProfileVerifyscore().waitForVisible();
		Validator.verifyThat("Verify Total Score", getHomepageProfileVerifyscore().getText(),
				Matchers.containsString("0"));
	}

	public void verifyProfile() {
		getHomepageProfileIcon().waitForVisible();
		getHomepageProfileIcon().click();
		getHomepageProfileVerifyscore().waitForVisible();

		String userTitleName = getHomepageProfileUsernameTitleLbl().getText().substring(1,
				getHomepageProfileUsernameTitleLbl().getText().length() - 1);
		String userName[] = getHomepageProfileusernameLbl().getText().split(" ");

		Validator.verifyThat("Verify User profile", userTitleName, Matchers.containsString(userName[0]));
	}

	public void verifyIncompleteStatus() {

		getHomepageProfileIncompleteMessage().waitForVisible();

		boolean flag1 = getHomepageProfileIncompleteMessage().getText()
				.contains(ConfigurationManager.getBundle().getProperty("quiz.title").toString())
				&& getHomepageProfileIncompleteMessage().getText()
						.contains(ConfigurationManager.getBundle().getString("incomplete.message"));

		Validator.verifyThat("Verify Recent Activity Incomplete Status", flag1, Matchers.equalTo(true));

		
	}

}
