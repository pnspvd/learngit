package com.acttest.pages;

import java.util.List;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.StringMatcher;
import com.qmetry.qaf.automation.util.Validator;

public class YourcollectionsTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "yourcollectionpage.navigationtab.headerlist")
	private List<QAFWebElement> yourcollectionpageNavigationtabHeaderlist;

	@FindBy(locator = "yourcollectionpage.view.lable")
	private QAFWebElement yourcollectionpageViewLable;

	@FindBy(locator = "yourcollectionpage.view.unuseful.option")
	private QAFWebElement yourcollectionpageViewUnusefulOption;

	@FindBy(locator = "yourcollectionpage.list.unusefulquestions")
	private List<QAFWebElement> yourcollectionpageListUnusefulquestions;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {

	}

	public List<QAFWebElement> getYourcollectionpageListUnusefulquestions() {
		return yourcollectionpageListUnusefulquestions;
	}

	public List<QAFWebElement> getYourcollectionpageNavigationtabHeaderlist() {
		return yourcollectionpageNavigationtabHeaderlist;
	}

	public QAFWebElement getYourcollectionpageViewLable() {
		return yourcollectionpageViewLable;
	}

	public QAFWebElement getYourcollectionpageViewUnusefulOption() {
		return yourcollectionpageViewUnusefulOption;
	}

	public void verifyYourCollectionPage() {
		getYourcollectionpageNavigationtabHeaderlist().get(0).waitForVisible();
		waitForPageToLoad();
		Validator.verifyThat("Verify Setting Page", getYourcollectionpageNavigationtabHeaderlist().size(),
				Matchers.greaterThan(0));
	}

	public void selectPreviouslySelectedQuiz(int index) {
		getYourcollectionpageNavigationtabHeaderlist().get(index).waitForVisible();
		getYourcollectionpageNavigationtabHeaderlist().get(index).click();
	}

	public void clickOnView() {
		getYourcollectionpageViewLable().waitForVisible();
		getYourcollectionpageViewLable().click();
		getYourcollectionpageViewUnusefulOption().waitForVisible();
		getYourcollectionpageViewUnusefulOption().click();
	}

	public void verifyMasteredQuestion() {
		getYourcollectionpageListUnusefulquestions().get(0).waitForVisible();
		if (getYourcollectionpageListUnusefulquestions().size() > 0) {
			for (int i = 0; i < getYourcollectionpageListUnusefulquestions().size(); i++) {
				if (getYourcollectionpageListUnusefulquestions().get(i).getText()
						.equals(ConfigurationManager.getBundle().getProperty("mastered.question"))) {
					getYourcollectionpageListUnusefulquestions().get(i).verifyText(StringMatcher.containsIgnoringCase(
							ConfigurationManager.getBundle().getProperty("mastered.question").toString()));
				}
			}
		}

	}

}
