package com.acttest.pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class QuizhomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "quizpage.title.label")
	private QAFWebElement quizpageTitleLabel;
	
	@FindBy(locator = "quizpage.button.start")
	private QAFWebElement quizpageButtonStart;
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getQuizpageTitleLabel() {
		return quizpageTitleLabel;
	}

	public QAFWebElement getQuizpageButtonStart() {
		return quizpageButtonStart;
	}
	
	public void verifyQuizHomePage(){
		waitForPageToLoad();
		Validator.verifyThat("Verify Quiz Home Page", getQuizpageTitleLabel().getText(),
				Matchers.containsString("Take a quiz"));
	}
	
	public void clickOnStartButton(){
		getQuizpageButtonStart().waitForVisible();
		getQuizpageButtonStart().click();
	}
}
