package com.acttest.pages;

import org.hamcrest.Matchers;

import com.acttest.utils.DriverUtils;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class SettingTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "settingpage.title.lable")
	private QAFWebElement settingpageTitleLable;
	@FindBy(locator = "settingpage.resetdata.label")
	private QAFWebElement settingpageResetdataLabel;
	@FindBy(locator = "settingpage.resetdata.confirmpopup")
	private QAFWebElement settingpageResetdataConfirmpopup;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getSettingpageTitleLable() {
		return settingpageTitleLable;
	}

	public QAFWebElement getSettingpageResetdataLabel() {
		return settingpageResetdataLabel;
	}

	public QAFWebElement getSettingpageResetdataConfirmpopup() {
		return settingpageResetdataConfirmpopup;
	}

	public void verfiySettingPage() {
		waitForPageToLoad();
		Validator.verifyThat("Verify Setting Page", getSettingpageTitleLable().getText(),
				Matchers.containsString("Setting"));
	}

	public void scrollTo() {
		DriverUtils.getAppiumDriver().scrollTo(getSettingpageResetdataLabel().getText());
	}

	public void clickOnResetData() {
		getSettingpageResetdataLabel().waitForVisible();
		getSettingpageResetdataLabel().click();
		getSettingpageResetdataConfirmpopup().waitForVisible();
		getSettingpageResetdataConfirmpopup().click();
		DriverUtils.getAppiumDriver().navigate().back();
	}

}
