package com.acttest.pages;

import org.hamcrest.Matchers;

import com.acttest.utils.DriverUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.StringMatcher;
import com.qmetry.qaf.automation.util.Validator;

public class QuizdetailTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "quizdetail.question.info")
	private QAFWebElement quizdetailQuestionInfo;

	@FindBy(locator = "quizdetail.icon.bookmark")
	private QAFWebElement quizdetailIconBookmark;

	@FindBy(locator = "quizdetail.bookmark.unuseful")
	private QAFWebElement quizdetailBookmarkUnuseful;

	@FindBy(locator = "quizdetail.popup.confirmation")
	private QAFWebElement quizdetailPopupConfirmation;

	@FindBy(locator = "quizdetail.popup.message")
	private QAFWebElement quizdetailPopupMessage;

	@FindBy(locator = "quizdetail.header.title")
	private QAFWebElement quizdetailHeaderTitle;

	public QAFWebElement getQuizdetailHeaderTitle() {
		return quizdetailHeaderTitle;
	}

	public QAFWebElement getQuizdetailQuestionInfo() {
		return quizdetailQuestionInfo;
	}

	public QAFWebElement getQuizdetailIconBookmark() {
		return quizdetailIconBookmark;
	}

	public QAFWebElement getQuizdetailPopupConfirmation() {
		return quizdetailPopupConfirmation;
	}

	public QAFWebElement getQuizdetailBookmarkUnuseful() {
		return quizdetailBookmarkUnuseful;
	}

	public QAFWebElement getQuizdetailPopupMessage() {
		return quizdetailPopupMessage;
	}

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public void verifyQuizDetailPage() {
		waitForPageToLoad();
		ConfigurationManager.getBundle().addProperty("quiz.title", getQuizdetailHeaderTitle().getText());
		Validator.verifyThat("Verify Setting Page", getQuizdetailQuestionInfo().isPresent(), Matchers.equalTo(true));
	}

	public void getQuestionInfo() {
		getQuizdetailQuestionInfo().waitForVisible();
		ConfigurationManager.getBundle().addProperty("mastered.question", getQuizdetailQuestionInfo().getText());
		Reporter.log(getQuizdetailQuestionInfo().getText(), MessageTypes.Pass);
	}

	public void clickOnBookmarkIcon() {
		getQuizdetailIconBookmark().waitForVisible();
		getQuizdetailIconBookmark().click();
		getQuizdetailBookmarkUnuseful().waitForVisible();
		getQuizdetailBookmarkUnuseful().click();
		DriverUtils.getAppiumDriver().navigate().back();
		getQuizdetailPopupConfirmation().waitForVisible();
		getQuizdetailPopupConfirmation().click();
	}

	public void handleDailogue() {
		DriverUtils.getAppiumDriver().navigate().back();
		getQuizdetailPopupMessage().waitForVisible();
		getQuizdetailPopupMessage().verifyText(
				StringMatcher.containsIgnoringCase(ConfigurationManager.getBundle().getString("popup.message")));
		getQuizdetailPopupConfirmation().waitForVisible();
		getQuizdetailPopupConfirmation().click();
	}
}
